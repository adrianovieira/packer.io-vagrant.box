# Install docker and docker-compose

if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then

  cat <<EOF >/etc/yum.repos.d/docker.repo
[dockerrepo]
name=Docker Repository (main)
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=0
gpgkey=https://yum.dockerproject.org/gpg
EOF

  yum install -y docker-engine-1.12.6 docker-engine-selinux-1.12.6
  curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
  systemctl enable docker
  usermod -aG docker vagrant

elif [[ "$ID" == "debian" ]]; then

  apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

  cat <<EOF >/etc/apt/sources.list.d/docker.list
deb https://apt.dockerproject.org/repo debian-jessie main
#deb https://apt.dockerproject.org/repo debian-jessie testing
#deb https://apt.dockerproject.org/repo debian-jessie experimental
EOF

  apt-get update

  apt-get install -y --force-yes docker-engine=1.12.6-0~debian-jessie
  curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
  usermod -aG docker vagrant
fi
