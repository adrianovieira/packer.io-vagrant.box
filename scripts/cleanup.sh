# Clean up
if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

rm -rf /tmp/*

if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then
  rm -f /root/VBoxGuestAdditions.iso
  rm -rf /var/log/*
  yum -y autoremove katello-ca-consumer*
  yum clean all
  if [[ "$ID" == "rhel"  ]]; then
    subscription-manager unregister
  fi
  rm -rf /var/cache/yum

elif [[ "$ID" == "debian" ]]; then
  apt-get -y autoremove
  apt-get -y clean

  # Remove debian cdrom from sources.list
  sed -i '/^deb cdrom:/s/^/#/' /etc/apt/sources.list

  # Removing leftover leases and persistent rules
  echo "cleaning up dhcp leases"
  rm -rf /var/lib/dhcp/*

  # Make sure Udev doesn't block our network
  echo "cleaning up udev rules"
  rm -rf /etc/udev/rules.d/70-persistent-net.rules
  mkdir /etc/udev/rules.d/70-persistent-net.rules
  rm -rf /dev/.udev/
  rm -rf /lib/udev/rules.d/75-persistent-net-generator.rules

  echo "Adding a 2 sec delay to the interface up, to make the dhclient happy"
  echo "pre-up sleep 2" >> /etc/network/interfaces

  # Remove iso
  rm -rf /root/debian-8.8.0-amd64-DVD-1.iso
  rm -rf /root/VBoxGuestAdditions.iso
  rm -rf /home/vagrant/debian-8.8.0-amd64-DVD-1.iso
  rm -rf /home/vagrant/VBoxGuestAdditions.iso
fi
