# Clean up
if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

rm -rf /tmp/*

if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then
  yum -y autoremove gcc make bzip2 kernel-headers kernel-devel-`uname -r` dkms kernel-devel-3.10.0-229.el7 kernel-3.10.0-229.el7 katello-ca-consumer*
elif [[ "$ID" == "debian" ]]; then
  apt-get -y remove linux-headers-$(uname -r) build-essential
  apt-get -y autoremove
  apt-get -y clean
fi
