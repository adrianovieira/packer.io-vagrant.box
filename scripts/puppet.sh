# This script installs puppet-agent

if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then

  yum install -y http://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
  yum install -y puppet-agent-1.10.8

elif [[ "$ID" == "debian" ]]; then

  apt-get install -y --force-yes puppet=1.10.8-0~debian-jessie
fi
