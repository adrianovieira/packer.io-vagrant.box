# This script installs puppet-agent

if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then

  yum install -y http://yum.puppetlabs.com/puppet/puppet5-release-el-7.noarch.rpm
  yum install -y puppet-agent-5.3.2

elif [[ "$ID" == "debian" ]]; then

  apt-get install -y --force-yes puppet=5.3.2-0~debian-jessie
fi
