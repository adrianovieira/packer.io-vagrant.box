# Update the box

# Remove debian cdrom from sources.list
sed -i '/^deb cdrom:/s/^/#/' /etc/apt/sources.list

# Add debian sources.list
cat <<EOF >>/etc/apt/sources.list.d/debian-jessie.list
deb  http://deb.debian.org/debian jessie main contrib non-free
EOF

# Set up sudo
echo 'vagrant ALL=NOPASSWD:ALL' > /etc/sudoers.d/vagrant

# Tweak sshd to prevent DNS resolution (speed up logins)
echo 'UseDNS no' >> /etc/ssh/sshd_config
