# Install Ruby from packages

. /etc/os-release

if [[ "$ID" == "centos" || "$VERSION" == "7"  ]]; then

  # installing ruby
  #yum install -y https://github.com/feedforce/ruby-rpm/releases/download/2.1.6/ruby-2.1.6-2.el7.centos.x86_64.rpm
  #yum install -y https://github.com/feedforce/ruby-rpm/releases/download/2.2.4/ruby-2.2.4-1.el7.centos.x86_64.rpm
  #yum install -y https://github.com/feedforce/ruby-rpm/releases/download/2.3.5/ruby-2.3.5-1.el7.centos.x86_64.rpm
  yum install -y https://github.com/feedforce/ruby-rpm/releases/download/2.4.2/ruby-2.4.2-1.el7.centos.x86_64.rpm
  gem install bundler

  # installing base devel tools then you can build gems
  yum install -y git make gcc gcc-c++ libxml2-devel libxslt-devel

  # installing phantomjs
  yum install -y bzip2 fontconfig
  curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 -o /tmp/phantomjs-2.1.1-linux-x86_64.tar.bz2
  tar jxvf /tmp/phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /tmp/  phantomjs-2.1.1-linux-x86_64/bin/phantomjs
  mv /tmp/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin
  yum autoremove -y bzip2
  rm -rf /tmp/*

elif [[ "$ID" == "debian" ]]; then

  apt-get -y install ruby ruby-dev irb ri rdoc rubygems
  # Install Rubygems from source
  #rg_ver=1.8.22
  #curl -o /tmp/rubygems-${rg_ver}.zip \
  #  "http://production.cf.rubygems.org/rubygems/rubygems-${rg_ver}.zip"
  #(cd /tmp && unzip rubygems-${rg_ver}.zip && \
  #  cd rubygems-${rg_ver} && ruby setup.rb --no-format-executable)
  #rm -rf /tmp/rubygems-${rg_ver} /tmp/rubygems-${rg_ver}.zip
  # This script installs puppet-agent

fi
