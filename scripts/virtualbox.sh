# This script installs and configure Virtual Box Guest Additions

if test -f .vbox_version ; then

  if [[ -f /etc/os-release  ]]; then
    . /etc/os-release
  fi

  if [[ "$ID" == "centos" || "$ID" == "rhel"  ]]; then
    yum -y install gcc make bzip2 kernel-headers kernel-devel-`uname -r` dkms
    mount -o loop /root/VBoxGuestAdditions.iso /mnt
    sh /mnt/VBoxLinuxAdditions.run
    umount /mnt
    rm -f /root/VBoxGuestAdditions.iso

  elif [[ "$ID" == "debian" ]]; then
    # The netboot installs the VirtualBox support (old) so we have to remove it
    if test -f /etc/init.d/virtualbox-ose-guest-utils ; then
      /etc/init.d/virtualbox-ose-guest-utils stop
    fi

    # Install dkms for dynamic compiles
    apt-get install -y --force-yes dkms

    # If libdbus is not installed, virtualbox will not autostart
    apt-get -y install --no-install-recommends libdbus-1-3

    # Install the VirtualBox guest additions
    VBOX_ISO=VBoxGuestAdditions.iso
    mount -o loop $VBOX_ISO /mnt
    yes|sh /mnt/VBoxLinuxAdditions.run
    umount /mnt
  fi

fi
