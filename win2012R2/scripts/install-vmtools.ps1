# VirtualBox Guest Additions (tools) Install
$host.UI.RawUI.set_WindowTitle("VirtualBox Guest Additions (tools) Install")

## Install Oracle Certificate
certutil -addstore -f "TrustedPublisher" A:\oracle-vbox.cer

## package install
if ((Test-Path "E:\VBoxWindowsAdditions.exe")) {
  E:\VBoxWindowsAdditions.exe /S /v"/qn REBOOT=R\"
  Write-Output "VirtualBox Guest Additions (tools) installed"
} else {
  Write-Output "VirtualBox Guest Additions (tools) NOT installed"
}

exit
