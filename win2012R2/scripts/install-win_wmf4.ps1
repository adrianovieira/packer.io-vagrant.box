# Windows Management Framework 4.0
$host.UI.RawUI.set_WindowTitle("Windows Management Framework 4.0
 Install")

if(!($PSVersionTable.PSVersion.Major -ge 4 )) {
  ## Powershell version not >= 4
  ## Windows Management Framework 4.0 package install
  if (!(Test-Path "C:\Windows\Temp\Windows6.1-KB2819745-x64-MultiPkg.msu")) {
    ### Download only if file not found
    $download_vendor_url = "https://download.microsoft.com/download/3/D/6/3D61D262-8549-4769-A660-230B67E15B25/Windows6.1-KB2819745-x64-MultiPkg.msu"
    Write-Output "Downloading $download_vendor_url"
    (New-Object System.Net.WebClient).DownloadFile($download_vendor_url, "C:\Windows\Temp\Windows6.1-KB2819745-x64-MultiPkg.msu")
  }

  if ((Test-Path "C:\Windows\Temp\Windows6.1-KB2819745-x64-MultiPkg.msu")) {
    C:\Windows\Temp\Windows6.1-KB2819745-x64-MultiPkg.msu
    Write-Output "Windows Management Framework 4.0 installed"
  }

}

exit
