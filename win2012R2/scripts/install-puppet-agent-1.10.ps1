# Puppet Install
$host.UI.RawUI.set_WindowTitle("Puppet Install")

## package download
### Only download if file not found
if (!(Test-Path "C:\Windows\Temp\puppet-agent-1.10.7-x64.msi")) {
  $download_vendor_url = "https://downloads.puppetlabs.com/windows/puppet-agent-1.10.7-x64.msi"
  Write-Output "Downloading $download_vendor_url"
  (New-Object System.Net.WebClient).DownloadFile($download_vendor_url, "C:\Windows\Temp\puppet-agent-1.10.7-x64.msi")
}

## package install
if ((Test-Path "C:\Windows\Temp\puppet-agent-1.10.7-x64.msi")) {
  Write-Output "Installing Puppet"
  msiexec /qn /norestart /i c:\Windows\Temp\puppet-agent-1.10.7-x64.msi PUPPET_MASTER_SERVER=puppet
  Write-Output "Puppet installed"
}

exit
