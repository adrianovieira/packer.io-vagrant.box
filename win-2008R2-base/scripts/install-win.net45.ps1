# Windows .Net Framework 4.5 (WMF 4.0 requirement)
$host.UI.RawUI.set_WindowTitle("Windows Management Framework 4.0
 Install")

if(!($PSVersionTable.PSVersion.Major -ge 4 )) {
  ## Powershell version not >= 4
  ## .Net Framework 4.5 package install
  if (!(Test-Path "C:\Windows\Temp\downloads\NDP452-KB2901907-x86-x64-AllOS-ENU.exe")) {
    ### Download only if file not found
    $download_vendor_url = "https://download.microsoft.com/download/E/2/1/E21644B5-2DF2-47C2-91BD-63C560427900/NDP452-KB2901907-x86-x64-AllOS-ENU.exe"
    Write-Output "Downloading $download_vendor_url"
    (New-Object System.Net.WebClient).DownloadFile($download_vendor_url, "C:\Windows\Temp\downloads\NDP452-KB2901907-x86-x64-AllOS-ENU.exe")
  }

  if ((Test-Path "C:\Windows\Temp\downloads\NDP452-KB2901907-x86-x64-AllOS-ENU.exe")) {
    cmd /c C:\Windows\Temp\downloads\NDP452-KB2901907-x86-x64-AllOS-ENU.exe /quiet
    Write-Output "Windows .Net Framework 4.5 installed"
  }

}
