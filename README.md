# Packerize boxes build

#### Table of contents

1. [Overview](#overview)
1. [Technologies](#technologies)
1. [Authors](#authors)
1. [Contributors](#contributors)
1. [Requirements](#requirements)
1. [Build Boxes](#build-boxes)

## Overview

Using [Packer](http://packer.io) as a tool for boxes build.

## Technologies

- Packer (<http://www.packer.io>)

## Authors

* Adriano Vieira (adriano.svieira at gmail.com)

## Contributors

* TBD

## Requirements

- Virtualbox 4+ (<https://www.virtualbox.org>)
- Vagrant >= 1.7 (<https://vagrantup.com>) ***optional***

Denpending of what box you want to build:

- Virtualbox Guest Addictions ISO (check it your own Virtualbox installation)
- CEntOS-7 ([download the Minimal ISO](http://www.centos.org))
- Windows-2008.R2 ([download ISO](http://care.dlservice.microsoft.com//dl/download/7/5/E/75EC4E54-5B02-42D6-8879-D8D3A25FBEF7/7601.17514.101119-1850_x64fre_server_eval_en-us-GRMSXEVAL_EN_DVD.iso))

## Build Boxes

First of all we need to download *Packer* and O.S. ISO (ISOs download is to reduce the build time and mainly a timeout error "downloading on build").

Put those ISOs on the same directory of the O.S. to build. Then change respective ***`template.json`*** file to reflecte this.

Now we could execute ***`packer`*** build command to have our boxes.

- `packer build template.json`

## Taste

To taste (or test) your boxes you could use *Vagrant*.

Take a look at *Vagrant* documentation to see how to use boxes and how to test your own one which you just create. 

***keep CALMS and having fun***
